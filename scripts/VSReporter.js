﻿"use strict";

module.exports = {
    reporter: function (res) {
        var len = res.length;
        var str = "";

        res.forEach(function (r) {
            var file = r.file;
            var error = r.error;

            str += file.substr(4) + '(' + error.line + ',' + error.character + '): error JSHint: ' + error.reason + "\n";
        });

        if (str) {
            process.stdout.write(str);
        }
    }
};