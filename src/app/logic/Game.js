define(['io/Input', 'assets/AssetsManager', 'logic/GameController'], function (Input, AssetsManager, GameController) {
    'use strict';

    return function Game(canvas, renderer) {
        var self = this;

        var input = new Input(canvas);
        var assetsManager = new AssetsManager();

        var gameController = new GameController(renderer, input, assetsManager);

        self.load = function () {
            return assetsManager.loadAssets();
        };

        self.initialize = function () {
            gameController.initialize();
        };

        self.update = function (elapsedTime) {
            gameController.update(elapsedTime);
        };

        self.render = function () {
            renderer.clear();
            gameController.render();
        };
    };
});

