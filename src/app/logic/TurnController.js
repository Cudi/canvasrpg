define(['logic/StatefulController', 'util/Vector', 'util/Diag'], function (StatefulController, Vector, Diag) {
    'use strict';

    // todo: this class is in a dire need of refactor
    return function TurnController(character, renderer, finder, input, camera, map, assetsManager) {
        var self = this.merge(new StatefulController());

        var actionPoints = character.getActionPoints();

        // state machine configuration
        self.start()
            .then(moveCameraToCharacter)
            .then(waitForAction)
            .then(executeAction)
            .branch(apLeft, waitForAction, self.end);

        // each function represents different turn state

        function moveCameraToCharacter() {
            var path = camera.getCenter().sub(character.getPos());
            var dist = 0;
            var len = path.length();
            var dir = path.normalize();
            var speed = Math.max(1000, Math.min(200, len / 2));
            var delta = 0;

            this.update = function (elapsedTime) {
                delta = elapsedTime * speed;
                dist += delta;

                camera.move(dir.copy().mult(delta));

                return dist > len;
            }
        }

        // todo: this state is too complex, code should be splitted
        function waitForAction() {
            var hoverCell = new Vector(-1, -1);
            var hoverPath = { x: -1, y: -1, segments: null };

            var hoverTileInsideTexture = assetsManager.getAsset('hoverTileInside');
            var moveAreaTileTexture = assetsManager.getAsset('moveAreaTile');
            var hoverTileOutsideTexture = assetsManager.getAsset('hoverTileOutside');
            var pathTileTexture = assetsManager.getAsset('pathTile');

            this.init = function () {
                map.addRenderer(renderMoveArea);
                updateFinder();
            };

            this.update = function (elapsedTime) {
                var mouseState = input.getMouseState();
                var events = mouseState.getEvents();

                var viewport = renderer.getViewport();
                var activeDistance = 100;

                if ((mouseState.pos.x >= 0 && mouseState.pos.x < activeDistance) || mouseState.pos.x > viewport.w - activeDistance ||
                    (mouseState.pos.y >= 0 && mouseState.pos.y < activeDistance) || mouseState.pos.y > viewport.h - activeDistance) {
                    var center = new Vector(viewport.w / 2, viewport.h / 2);
                    var dir = mouseState.pos.copy().sub(center).normalize().mult(-200 * elapsedTime);

                    camera.move(dir);
                }

                hoverCell = mouseState.pos.copy().map(camera.screenToWorld).map(map.worldToMap);

                if (map.isInside(hoverCell) && (hoverPath.x !== hoverCell.x || hoverPath.y !== hoverCell.y)) {
                    hoverPath.x = hoverCell.x;
                    hoverPath.y = hoverCell.y;
                    hoverPath.segments = finder.getPath(hoverCell);
                }

                for (var i = 0; i < events.length; i++) {
                    var event = events[i];

                    if (event.name === 'click') {
                        var pos = camera.screenToWorld(event.data.x, event.data.y).map(map.worldToMap);

                        if (!map.isInside(pos)) {
                            continue;
                        }

                        var path = finder.getPath(pos);

                        if (!path) {
                            continue;
                        }

                        var node = finder.getNodeAt(pos.x, pos.y);

                        actionPoints -= Math.ceil(node.g);
                        path = path.from(1).map(function (v) { return v.map(map.mapToWorld); });

                        character.move(path);

                        return true;
                    }
                }

                Diag.set('characterPos', character.getPos().map(map.worldToMap));
            };

            this.deinit = function () {
                map.removeRenderer(renderMoveArea);
            }

            function updateFinder() {
                var playerPos = character.getPos().map(map.worldToMap);

                finder.updatePosition(playerPos, actionPoints);
            }

            function renderMoveArea(i, j, x, y) {
                var node = finder.getNodeAt(i, j);

                if (node.walkable) {
                    var isHover = hoverCell.x === i && hoverCell.y === j && !character.isMoving();
                    var isHoverPathSegment = node.g && hoverPath.segments && hoverPath.segments.any(function (s) { return s.x === i && s.y === j });

                    if (isHoverPathSegment) {
                        renderer.renderImage(pathTileTexture, x, y);
                    }

                    if (node.g <= character.getActionPoints()) {
                        renderer.renderImage(moveAreaTileTexture, x, y);
                    }
                }
            }
        }

        function executeAction() {
            this.update = function (elapsedTime) {
                var result = character.update(elapsedTime);

                if (result === 'moveEnd') {
                    return true;
                }
            }
        }

        function apLeft() {
            return actionPoints > 0;
        }
    };
});