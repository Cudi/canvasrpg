﻿define(function () {
    'use strict';

    return function Party() {
        var self = this;
        var characters = [];
        var activeCharacter = null;

        self.addCharacter = function (character) {
            characters.push(character);

            character.setParty(self);

            if (activeCharacter === null) {
                activeCharacter = character;
            }
        };

        self.getCharacters = function () {
            return characters;
        };

        self.setActiveCharacter = function (character) {
            activeCharacter = character;
        };

        self.getActiveCharacter = function () {
            return activeCharacter;
        };
    };
});