﻿define(function () {
    'use strict';

    // base class for controllers utilzing state machine
    return function StatefulController() {
        var self = this;
        var currentState;
        var states = [];
        var stateIndex = -1;

        self.start = function () {
            var stateMachine = {};

            stateMachine.then = function (state) {
                states.push({ state: state });

                return stateMachine;
            };

            stateMachine.branch = function (condition, ifTrue, ifFalse) {
                states.push({ condition: condition, ifTrue: ifTrue, ifFalse: ifFalse });

                return stateMachine;
            };

            return stateMachine;
        }

        self.end = function () {

        };

        self.update = function (elapsedTime) {
            if (!currentState) {
                stateIndex++;

                var nextState = states[stateIndex];

                if (nextState.state) {
                    currentState = new nextState.state();
                } else if (nextState.condition) {
                    var result = nextState.condition();
                    var goto = result ? nextState.ifTrue : nextState.ifFalse;

                    if (goto === self.end) {
                        return 'turnEnd';
                    } else {
                        stateIndex = states.findIndex(function (state) { return state.state === goto });
                    }

                    currentState = new states[stateIndex].state();
                } else {
                    throw new Error('State machine has no states to go to... poor, sad state machine :(')
                }

                if (currentState.init) {
                    currentState.init();
                }
            }

            var hasFinished = currentState.update(elapsedTime);

            if (hasFinished) {
                if (currentState.deinit) {
                    currentState.deinit();
                }

                currentState = null;
            }
        };

        self.render = function () {
            if (currentState && currentState.render) {
                currentState.render();
            }
        };
    }
});