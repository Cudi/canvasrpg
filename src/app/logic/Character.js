define(['util/Vector', 'util/Size'], function (Vector, Size) {
    'use strict';

    return function Character() {
        var self = this;
        var pos = new Vector(0, 0);
        var size = new Size(32, 32);
        var speed = 60;
        var heading = 0;
        var dir = null;
        var dest = null;
        var steps = null;
        var stepIndex = 0;
        var party = null;

        // stats
        var actionPoints = 10;
        var initiative = 10;

        self.getActionPoints = function () {
            return actionPoints;
        };

        self.getInitiative = function () {
            return initiative;
        };

        self.getPos = function () {
            return pos.copy();
        };

        self.setPos = function (p) {
            pos = p.copy();
        };

        self.isMoving = function () {
            return dir !== null;
        };

        self.setParty = function (_party) {
            party = _party;
        };

        self.getParty = function () {
            return party;
        };

        self.move = function (s) {
            steps = s;
            stepIndex = 0;

            steps.unshift(pos.copy());

            nextStep();
        };

        var stepElapsedTime, stepTotalTime;

        self.update = function (elapsedTime) {
            if (dir) {
                stepElapsedTime += elapsedTime;

                var progress = stepElapsedTime / stepTotalTime;

                console.log('progress %f', progress);

                if (progress >= 1) {
                    if (!nextStep()) {
                        return 'moveEnd';
                    }
                } else {
                    var start = steps[stepIndex - 1];
                    var end = steps[stepIndex];

                    pos.x = lerp(start.x, end.x, progress);
                    pos.y = lerp(start.y, end.y, progress);
                }
            }
        };

        function nextStep() {
            stepIndex++;

            if (stepIndex < steps.length) {
                var v = steps[stepIndex];

                dest = v;
                dir = dest.copy().sub(pos).normalize();
                heading = dir.toAngle();

                stepElapsedTime = 0;
                stepTotalTime = (v.copy().sub(steps[stepIndex - 1]).length()) / speed;

                return true;
            } else {
                dir = null;

                return false;
            }
        }

        function lerp(v0, v1, t) {
            return v0 + (v1 - v0) * t;
        }
    };
});