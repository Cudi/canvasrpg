define(['util/Camera', 'logic/Party', 'logic/BattleController', 'map/MapGenerator', 'util/Vector', 'map/Pathfinder', 'logic/Character'],
    function (Camera, Party, BattleController, MapGenerator, Vector, Pathfinder, Character) {
    'use strict';

    return function GameController(renderer, input, assetsManager) {
        var self = this;

        var camera = new Camera(renderer);

        var party = new Party();
        var enemies = new Party();

        var map;
        var finder;

        // todo: move to configuration file
        var debugOptions = {
            displayGrid: true,
            displayGridCoords: false,
            displayMoveDistance: false,
        };

        var stateController;
        var characterActiveTexture, characterInactiveTexture;

        self.initialize = function () {
            initializeMap();
            initializeParty();

            stateController = new BattleController([party, enemies], renderer, finder, input, camera, map, assetsManager);
        };

        self.update = function (elapsedTime) {
            stateController.update(elapsedTime);
        };

        self.render = function () {
            var cameraOffset = camera.getOffset();

            renderer.setTranslation(cameraOffset.x, cameraOffset.y);

            var viewport = renderer.getViewport();

            var tlc = camera.screenToWorld(0, 0).map(map.worldToMap).add(new Vector(-1, 0));
            var brc = camera.screenToWorld(viewport.w, viewport.h).map(map.worldToMap).add(new Vector(3, 0));

            map.render(renderer, assetsManager, tlc, brc);

            stateController.render(renderer);

            var activeCharacter = party.getActiveCharacter();

            party.getCharacters().each(function (character) {
                var pos = character.getPos();
                var characterTexture = character === activeCharacter ? characterActiveTexture : characterInactiveTexture;

                renderer.renderImage(characterTexture, pos.x - (characterTexture.width / 2), pos.y - (characterTexture.height / 2));
            });
        };

        function initializeMap() {
            var mapGenerator = new MapGenerator();

            map = mapGenerator.randomMap(100, 100, 0.1);
            finder = new Pathfinder(map);
        }

        function initializeParty() {
            var mapSize = map.getSize();
            var characterPos = new Vector(mapSize.w / 2, mapSize.h / 2);

            party.addCharacter(new Character());
            party.addCharacter(new Character());
            party.addCharacter(new Character());

            party.getCharacters().each(function (character) {
                character.setPos(characterPos.translate(new Vector(2, 0)).copy().map(map.mapToWorld));
            });

            characterPos.translate(new Vector(-2 * party.getCharacters().length, 2));

            characterActiveTexture = assetsManager.getAsset('characterActive');
            characterInactiveTexture = assetsManager.getAsset('characterInactive');
        }
    };
});