define(['logic/TurnController'], function (TurnController) {
    'use strict';

    return function BattleController(parties, ctx, finder, input, camera, map, assetsManager) {
        var self = this;

        var characters = parties.map(function (party) { return party.getCharacters(); })
            .flatten()
            .sort(function (character) { return character.getInitiative(); });

        var currentIndex = -1;
        var turnController = null;

        camera.centerAt(characters[0].getPos());
        nextTurn();

        self.update = function (elapsedTime) {
            var result = turnController.update(elapsedTime);

            if (result === 'turnEnd') {
                nextTurn();
            }
        };

        self.render = function (ctx) {
            turnController.render(ctx);
        };

        function nextTurn() {
            currentIndex = ++currentIndex % characters.length;

            var character = characters[currentIndex];
            var party = character.getParty();

            party.setActiveCharacter(character);

            turnController = new TurnController(character, ctx, finder, input, camera, map, assetsManager);
        }
    };
});