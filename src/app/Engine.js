define(['logic/Game', 'util/Renderer', 'util/Diag'], function (Game, Renderer, Diag) {
    'use strict';

    var Engine = function Engine() {
        var self = this;
        var requestId = null;

        Diag.init();

        var canvas = document.getElementById('game');
        var renderer = new Renderer(canvas);
        var game = new Game(canvas, renderer);

        var requestAnimationFrame = window.requestAnimationFrame || window.mozRequestAnimationFrame;
        var cancelAnimationFrame = window.requestAnimationFrame || window.mozCancelAnimationFrame;

        var maxWidth = 640;
        var maxHeight = 480;

        var lastUpdate;

        updateCanvasSize();
        game.load().then(start);

        function start() {
            try {
                game.initialize();
                renderer.initialize();

                lastUpdate = new Date().getTime();
                requestAnimationFrame(step);
            } catch (error) {
                console.error(error);
            }
        }

        function step() {
            var now = new Date().getTime();
            var elapsedTime = (now - lastUpdate) / 1000;
            lastUpdate = now;

            renderer.clear();

            game.update(elapsedTime);
            game.render(elapsedTime);

            requestId = requestAnimationFrame(step);
        }

        self.pause = function () {
            if (requestId) {
                cancelAnimationFrame(requestId);
                requestId = null;
            } else {
                start();
            }
        };

        function updateCanvasSize() {
            canvas.width = Math.min(maxWidth, window.innerWidth);
            canvas.height = Math.min(maxHeight, window.innerHeight);

            renderer.updateViewport();
        }

        window.addEventListener('resize', updateCanvasSize, true);
    };

    return Engine;
});
