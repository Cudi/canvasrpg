﻿define(function () {
    'use strict';

    function Diag() {
        var elem = null;
        var store = {};

        this.init = function () {
            elem = document.getElementById('diag');

            update = update.throttle(200);

            update();
        };

        this.set = function (key, value) {
            value = JSON.stringify(value, null, 4);

            if (!store[key] || store[key].value !== value) {
                store[key] = value;
                update();
            }
        };

        function update() {
            if (!elem)
                return;

            var html = '<ul>';

            store.each(function (key, value) {
                html += '<li>' + key + ': ' + value + '</li>';
            });

            html += '</ul>';

            elem.innerHTML = html;
        }
    }

    return new Diag();
});