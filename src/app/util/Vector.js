﻿define(function () {
    'use strict';

    function Vector(x, y) {
        this.x = x || 0;
        this.y = y || 0;
    }

    Vector.prototype.copy = function () {
        return new Vector(this.x, this.y);
    };

    Vector.prototype.translate = function (x, y) {
        if (x instanceof Vector) {
            this.x += x.x;
            this.y += x.y;
        } else if (x instanceof Function) {
            var value = x.call(this);

            this.x += value[0];
            this.y += value[1];
        } else {
            this.x += x;
            this.y += y;
        }

        return this;
    };

    Vector.prototype.normalize = function () {
        var len = this.length();

        this.x /= len;
        this.y /= len;

        return this;
    };

    Vector.prototype.map = function (mapX, mapY) {
        if (mapX.length === 1) {
            this.x = mapX(this.x);
            this.y = mapY ? mapY(this.y) : mapX(this.y);
        } else {
            var result = mapX(this.x, this.y);

            this.x = result.x;
            this.y = result.y;
        }

        return this;
    };

    Vector.prototype.toAngle = function () {
        return Math.atan2(this.y, this.x);
    };

    Vector.prototype.length = function () {
        return Math.sqrt(this.x * this.x + this.y * this.y);
    };

    Vector.prototype.isZero = function () {
        return this.x === 0 && this.y === 0;
    };

    Vector.prototype.add = function (otherOrX, y) {
        if (y) {
            this.x += otherOrX;
            this.y += y;

        } else {
            this.x += otherOrX.x;
            this.y += otherOrX.y;
        }

        return this;
    };

    Vector.prototype.mult = function (val) {
        this.x *= val;
        this.y *= val;

        return this;
    };

    Vector.prototype.sub = function (other) {
        this.x -= other.x;
        this.y -= other.y;

        return this;
    };

    Vector.prototype.clamp = function (maxX, maxY) {
        this.x -= other.x;
        this.y -= other.y;

        return this;
    };

    return Vector;
});