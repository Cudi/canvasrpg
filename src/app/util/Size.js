﻿define(function () {
    'use strict';

    function Size(w, h) {
        this.w = w || 0;
        this.h = h || 0;
    }

    return Size;
});