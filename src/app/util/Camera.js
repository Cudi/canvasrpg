define(['util/Vector'], function (Vector) {
    'use strict';

    return function Camera(renderer) {
        var offset = new Vector(0, 0);

        this.centerAt = function (pos) {
            var viewport = renderer.getViewport();

            offset.x = (viewport.w / 2) - pos.x;
            offset.y = (viewport.h / 2) - pos.y;
        };

        this.move = function (v) {

            offset.add(v);
        };

        this.getCenter = function () {
            var viewport = renderer.getViewport();

            return this.screenToWorld(viewport.w / 2, viewport.h / 2);
        };

        this.getOffset = function () {
            return offset;
        };

        this.screenToWorld = function (x, y) {
            return new Vector(x - offset.x, y - offset.y);
        };
    };
});
