define(['util/Size'], function (Size) {
    'use strict';

    return function Renderer(canvas) {
        var gl = canvas.getContext('webgl') || canvas.getContext('experimental-webgl');
        var program = gl.createProgram();

        var textureCache = {};
        var translation = [0, 0];

        var transformMatrix = [
            1, 0, 0,
            0, 1, 0,
            0, 0, 1
        ];

        this.initialize = function () {
            gl.attachShader(program, createVertexShader());
            gl.attachShader(program, createFragmentShader());
            gl.linkProgram(program);

            if (!gl.getProgramParameter(program, gl.LINK_STATUS)) {
                throw new Error("Unable to initialize the shader program.");
            }

            gl.useProgram(program);

            gl.enable(gl.BLEND);
            gl.blendEquationSeparate(gl.FUNC_ADD, gl.FUNC_ADD);
            gl.blendFuncSeparate(gl.SRC_ALPHA, gl.ONE_MINUS_SRC_ALPHA, gl.ONE, gl.ONE_MINUS_SRC_ALPHA);

            program.resolutionLocation = gl.getUniformLocation(program, "u_resolution");
            program.positionLocation = gl.getAttribLocation(program, "a_position");
            program.texCoordLocation = gl.getAttribLocation(program, "a_texCoord");
            program.matrixLocation = gl.getUniformLocation(program, "u_matrix");

            gl.viewport(0, 0, canvas.width, canvas.height);
            gl.uniform2f(program.resolutionLocation, canvas.width, canvas.height);

            gl.bindBuffer(gl.ARRAY_BUFFER, gl.createBuffer());
            gl.bufferData(gl.ARRAY_BUFFER, new Float32Array([0.0, 0.0, 1.0, 0.0, 0.0, 1.0, 0.0, 1.0, 1.0, 0.0, 1.0, 1.0]), gl.STATIC_DRAW);
            gl.enableVertexAttribArray(program.positionLocation);
            gl.vertexAttribPointer(program.positionLocation, 2, gl.FLOAT, false, 0, 0);
            gl.enableVertexAttribArray(program.texCoordLocation);
            gl.vertexAttribPointer(program.texCoordLocation, 2, gl.FLOAT, false, 0, 0);
            gl.bindBuffer(gl.ARRAY_BUFFER, null);
        };

        this.clear = function () {
            gl.clear(gl.COLOR_BUFFER_BIT);
        };

        this.setTranslation = function (x, y) {
            translation[0] = x;
            translation[1] = y;
        }

        this.getViewport = function () {
            return new Size(canvas.width, canvas.height);
        };

        this.updateViewport = function () {
            if (program) {
                gl.viewport(0, 0, canvas.width, canvas.height);
                gl.uniform2f(program.resolutionLocation, canvas.width, canvas.height);
            }
        };

        this.renderImage = function (image, x, y) {
            transformMatrix[0] = image.width;
            transformMatrix[4] = image.height;
            transformMatrix[6] = translation[0] + x;
            transformMatrix[7] = translation[1] + y;

            gl.uniformMatrix3fv(program.matrixLocation, false, transformMatrix);
            gl.bindTexture(gl.TEXTURE_2D, textureCache[image.src] || createTexture(image));
            gl.drawArrays(gl.TRIANGLES, 0, 6);
        };

        function createTexture(image) {
            var texture = gl.createTexture();
            gl.bindTexture(gl.TEXTURE_2D, texture);

            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);

            gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, image);

            gl.bindTexture(gl.TEXTURE_2D, null);

            textureCache[image.src] = texture;

            return texture;
        }

        function createVertexShader() {
            return createShader(gl.VERTEX_SHADER, [
                "attribute vec2 a_position;",
                "attribute vec2 a_texCoord;",

                "uniform mat3 u_matrix;",
                "uniform vec2 u_resolution;",

                "varying vec2 v_texCoord;",

                "void main() {",
                "    vec2 projected = (u_matrix * vec3(a_position, 1)).xy;",
                "    vec2 normal = projected / u_resolution;",
                "    vec2 clipSpace = (normal * 2.0) - 1.0;",

                "    gl_Position = vec4(clipSpace * vec2(1, -1), 0, 1);",

                "    v_texCoord = a_texCoord;",
                "}",
            ].join('\n'));
        }

        function createFragmentShader() {
            return createShader(gl.FRAGMENT_SHADER, [
                "precision mediump float;",

                "uniform sampler2D u_image;",
                "varying vec2 v_texCoord;",

                "void main() {",
                "    gl_FragColor = texture2D(u_image, v_texCoord);",
                "}",
            ].join('\n'));
        }

        function createShader(type, source) {
            var shader = gl.createShader(type);

            gl.shaderSource(shader, source);
            gl.compileShader(shader);

            if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
                throw new Error("An error occurred compiling the shaders: " + gl.getShaderInfoLog(shader));
            }

            return shader;
        }
    };
});
