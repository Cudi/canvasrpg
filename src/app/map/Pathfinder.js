﻿define(['util/Vector', 'lib/pathfinding'], function (Vector, PF) {
    'use strict';

    // Based on pathfinding.js (https://github.com/qiao/PathFinding.js) A* algorithm implementation, 
    // reduced to 2-step Dijkstra algorithm
    return function Pathfinder(map) {
        var self = this;

        var mapSize = map.getSize();
        var origGrid = new PF.Grid(mapSize.w, mapSize.h, map.getData());
        var grid;

        self.getPath = function (dest) {
            var node = grid.getNodeAt(dest.x, dest.y);

            if (node.g) {
                var path = [new Vector(node.x, node.y)];

                while (node.parent) {
                    node = node.parent;
                    path.push(new Vector(node.x, node.y));
                }

                return path.reverse();
            }

            return null;
        };

        self.getNodeAt = function (c, r) {
            return grid.getNodeAt(c, r);
        };

        self.updatePosition = function (startPos, maxDistance) {
            grid = origGrid.clone();

            var openList = new PF.Heap(function (nodeA, nodeB) {
                return nodeA.g - nodeB.g;
            }),
                startNode = grid.getNodeAt(startPos.x, startPos.y),
                SQRT2 = Math.SQRT2,
                node, neighbors, neighbor, i, l, x, y, ng;

            // set the `g` value of the start node to be 0
            startNode.g = 0;

            // push the start node into the open list
            openList.push(startNode);
            startNode.opened = true;

            // while the open list is not empty
            while (!openList.empty()) {
                // pop the position of node which has the minimum `f` value.
                node = openList.pop();
                node.closed = true;

                // get neigbours of the current node
                neighbors = grid.getNeighbors(node, true, true);
                for (i = 0, l = neighbors.length; i < l; ++i) {
                    neighbor = neighbors[i];

                    if (neighbor.closed) {
                        continue;
                    }

                    x = neighbor.x;
                    y = neighbor.y;

                    // get the distance between current node and the neighbor
                    // and calculate the next g score
                    ng = node.g + ((x - node.x === 0 || y - node.y === 0) ? 1 : SQRT2);

                    // check if the neighbor has not been inspected yet, or
                    // can be reached with smaller cost from the current node
                    if (ng <= maxDistance && (!neighbor.opened || ng < neighbor.g)) {
                        neighbor.g = ng;
                        neighbor.parent = node;

                        if (!neighbor.opened) {
                            openList.push(neighbor);
                            neighbor.opened = true;
                        } else {
                            // the neighbor can be reached with smaller cost.
                            // Since its f value has been updated, we have to
                            // update its position in the open list
                            openList.updateItem(neighbor);
                        }
                    }
                } // end for each neighbor
            } // end while not open list empty
        };
    };
});