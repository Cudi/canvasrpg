﻿define(['map/Map'], function (Map) {
    'use strict';

    function MapGenerator() {

    }

    MapGenerator.prototype.randomMap = function (width, height, density) {
        function getRandomInt(min, max) {
            return Math.floor(Math.random() * (max - min + 1)) + min;
        }

        var map = new Array(height);
        var x, y;

        for (y = 0; y < height; y++) {
            map[y] = new Array(width);

            for (x = 0; x < width; x++) {
                map[y][x] = false;
            }
        }

        var fillCount = 0;
        var desiredFill = (density || 0.5) * width * height;

        while (fillCount < desiredFill) {
            x = getRandomInt(0, width - 1);
            y = getRandomInt(0, height - 1);

            if (!map[y][x]) {
                map[y][x] = true;
                fillCount++;
            }
        }

        return new Map(map, width, height);
    };

    return MapGenerator;
});