define(['util/Size', 'util/Vector'], function (Size, Vector) {
    'use strict';

    return function Map(data, width, height) {
        var self = this;

        var tileWidth = 64;
        var tileHeight = 32;
        var renderers = [];
        var gridTexture, wallTexture;

        self.getSize = function () {
            return new Size(width, height);
        };

        self.getTileSize = function () {
            return new Size(tileWidth, tileHeight);
        };

        self.getData = function () {
            return data;
        };

        self.isInside = function (pos) {
            return pos.x >= 0 && pos.x < width && pos.y >= 0 && pos.y < height;
        };

        self.isWalkable = function (pos) {
            return !data[pos.y][pos.x];
        };

        self.worldToMap = function (x, y) {
            x -= tileWidth / 2;
            y -= tileHeight / 2;

            var i = Math.round(x / tileWidth + y / tileHeight);
            var j = Math.round(y / tileHeight - x / tileWidth);

            return new Vector(i, j);
        };

        self.mapToWorld = function (i, j) {
            var x = i * tileWidth / 2 - j * tileWidth / 2 + (tileWidth / 2);
            var y = i * tileHeight / 2 + j * tileHeight / 2 + (tileHeight / 2);

            return new Vector(x, y);
        };

        self.addRenderer = function (renderer) {
            renderers.push(renderer);
        };

        self.removeRenderer = function (renderer) {
            renderers.splice(renderers.indexOf(renderer), 1);
        };

        self.render = function (renderer, assetsManager, tlc, brc) {
            if (!gridTexture) {
                gridTexture = assetsManager.getAsset('grid');
                wallTexture = assetsManager.getAsset('wall');
            }

            var s = tlc.map(self.mapToWorld).map(function (x, y) { return new Vector(x - tileWidth / 2, y - tileHeight / 2) });
            var e = brc.map(self.mapToWorld).map(function (x, y) { return new Vector(x - tileWidth / 2, y - tileHeight / 2) });

            for (var y = s.y; y <= e.y; y += tileHeight) {
                for (var x = s.x; x <= e.x; x += tileWidth) {
                
                    var i = x / tileWidth + y / tileHeight;
                    var j = y / tileHeight - x / tileWidth;

                    drawTile(renderer, assetsManager, i, j, x, y);
                    
                    if (x != e.x && y != e.y) {
                        drawTile(renderer, assetsManager, i, j + 1, x - tileWidth / 2, y + tileHeight / 2);
                    }
                }
            }
        };

        function drawTile(renderer, assetsManager, i, j, x, y) {
            if (i < 0 || j < 0 || i >= width || j >= height)
                return;

            if (data[j][i]) {
                renderer.renderImage(wallTexture, x, y);

            } else {
                renderer.renderImage(gridTexture, x, y);
            }

            for (var rendererIndex = 0; rendererIndex < renderers.length; rendererIndex++) {
                renderers[rendererIndex](i, j, x, y, tileWidth, tileHeight);
            }
        }
    };
});