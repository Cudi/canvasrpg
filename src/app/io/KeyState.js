﻿define(['util/Vector'], function (Vector) {
    'use strict';

    return function KeyState() {
        this.leftPressed = false;
        this.upPressed = false;
        this.rightPressed = false;
        this.downPressed = false;

        this.toVector = function () {
            var vector = new Vector(0, 0);

            if (this.leftPressed) vector.translate(-1, 0);
            if (this.upPressed) vector.translate(0, -1);
            if (this.rightPressed) vector.translate(1, 0);
            if (this.downPressed) vector.translate(0, 1);

            return vector;
        };
    };
});