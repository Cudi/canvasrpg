﻿define(['io/KeyState', 'io/MouseState'], function (KeyState, MouseState) {
    'use strict';

    return function Input(canvas) {
        window.addEventListener('keydown', handleKeyDown);
        window.addEventListener('keyup', handleKeyUp);
        canvas.addEventListener('click', handleClick);
        canvas.addEventListener('mousemove', handleMove);
        canvas.addEventListener('mouseout', handleOut);

        var keyState = new KeyState();
        var mouseState = new MouseState();

        var mapping = {
            left: 37,
            up: 38,
            right: 39,
            down: 40,
        };

        var reversedMapping = reverse(mapping);

        function reverse(dict) {
            var reversed = {};

            dict.each(function (key, value) {
                reversed[value] = key;
            });

            return reversed;
        }

        function handleKeyDown(e) {
            var keyName = reversedMapping[e.keyCode];

            if (!keyName)
                return;

            keyState[keyName + 'Pressed'] = true;
        }

        function handleKeyUp(e) {
            var keyName = reversedMapping[e.keyCode];

            if (!keyName)
                return;

            keyState[keyName + 'Pressed'] = false;
        }

        function handleClick(e) {
            mouseState.addEvent('click', { x: e.clientX, y: e.clientY });
        }

        function handleMove(e) {
            mouseState.pos.x = e.clientX;
            mouseState.pos.y = e.clientY;
        }

        function handleOut(e) {
            mouseState.pos.x = -1;
            mouseState.pos.y = -1;
        }

        this.getKeyState = function () {
            return keyState;
        };

        this.getMouseState = function () {
            return mouseState;
        };
    };
});