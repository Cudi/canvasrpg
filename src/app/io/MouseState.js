﻿define(['util/Vector'], function (Vector) {
    'use strict';

    return function MouseState() {
        var events = [];

        this.pos = new Vector(-1, -1);

        this.addEvent = function (name, data) {
            events.push({ name: name, data: data });
        };

        this.getEvents = function () {
            if (events.length > 0) {
                var evts = events;

                events = [];

                return evts;
            } else {
                return events;
            }
        };
    };
});