﻿define(['lib/q'], function (Q) {
    'use strict';

    return function AssetsManager() {
        var self = this;

        // this dictionary should be moved outside of this class to some kind of configuration file
        var assets = {
            grid: 'grid.png',
            wall: 'wall.png',
            characterActive: 'characterActive.png',
            characterInactive: 'characterInactive.png',
            hoverTileInside: 'hoverTileInside.png',
            hoverTileOutside: 'hoverTileOutside.png',
            pathTile: 'pathTile.png',
            moveAreaTile: 'moveAreaTile.png',
        };

        self.loadAssets = function () {
            return Q.all(assets.map(createLoader).values());
        };

        self.getAsset = function (key) {
            if (!self.logged) {
                self.logged = true;
            }
            return assets[key];
        };

        function createLoader(key, file) {
            var deferred = Q.defer();

            var image = new Image();
            image.onload = function () {
                assets[key] = image;
                deferred.resolve(key);
            };
            image.src = 'assets/' + file;

            return deferred.promise;
        }
    };
});