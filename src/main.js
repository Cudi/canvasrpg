requirejs.config({
    baseUrl: 'app',
    paths: {
        lib: '../lib'
    },
    shim: {
        'lib/pathfinding': {
            exports: 'PF'
        },
    }
});

requirejs(['Engine', 'lib/sugar'], function (Engine) {
    // initialize Sugar.js
    Object.extend();

    // initialize Engine at make it available in the global scope
    window.engine = new Engine();
});