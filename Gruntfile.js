﻿module.exports = function (grunt) {
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    jshint: {
      all: 'src/app/**/*.js',
      options: {
        reporter: 'scripts/VSReporter.js',
        globals: {
          define: true
        },
        es5: true,
        browser: true,
        undef: true,
        unused: true,
        strict: true,
        trailing: true,
        nonew: true,
        newcap: true,
        indent: 4,
        immed: true,
        forin: true,
        eqeqeq: true,
        bitwise: true,
        camelcase: true,
        noarg: true,
        quotmark: true,
        maxlen: 160,
      }
    },
    connect: {
      server: {
        options: {
          port: 8080,
          base: 'src',
          keepalive: true,
        }
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-connect');

  grunt.registerTask('default', ['jshint:all']);
  grunt.registerTask('server', ['connect:server']);
};